import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Persona } from 'src/app/models/persona';
import { ApiServiceService } from 'src/app/services/api-service.service';

declare var $: any;//JQuery

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.scss']
})
export class ReadComponent implements OnInit {

  personas: Persona[];

  constructor(
    private apiService: ApiServiceService,
    private spinner: NgxSpinnerService,
    private toastr:ToastrService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.apiService.getPeople().subscribe(data => {            
      if(data.length > 0){        
        this.personas = data        
      }
      this.spinner.hide();
    }, err => {
      console.log(err)
      this.spinner.hide();
    })
  }

  beforeDelete(person) {    
    $("#ModalTitle").empty();
    $("#ModalContent").empty();
    $("#buttonDelete").empty();
    $("#idPersonDelete").val()
    $("#ModalTitle").append("Elimnar")        
    $("#ModalContent").append("Desea eliminar el registro con el  ID: " + person.id + "<br> y el nombre: " + person.nombre + " " + person.apellidos);
    $("#idPersonDelete").val(person.id)
    //$("#ModalActions").append("<button type='button' class='btn btn-primary' (click)='deletePerson(" + person.id + ")'>Eliminar</button>");

  }

  deletePerson() {  
    let id = $("#idPersonDelete").val();    
    this.apiService.deletePerson(id).subscribe(data => {      
      let index = this.personas.findIndex(persona => persona.id === id)
      this.personas.splice(index, 1);
      this.toastr.success("Eliminación correcta", "Eliminacion")
      $('#Options').modal('toggle');
    }, err => {      
      this.toastr.error("Error durante el proceso. " + err,"Error")
    })
  }


}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiServiceService } from 'src/app/services/api-service.service';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

  id: any;
  error:any;
  successMessage:any;

  constructor(
    private paramsURL: ActivatedRoute,
    private form: FormBuilder,
    private apiService: ApiServiceService,
    private spinner: NgxSpinnerService,
    private toastr:ToastrService
  ) { }

  ngOnInit(): void {
    this.spinner.show()
    this.id = this.paramsURL.snapshot.paramMap.get('id');
    console.log(this.id)
    this.apiService.getPerson(this.id).subscribe(data=>{            
      this.formularioPersona.setValue({
        id:data.id,
        nombre: data.nombre,
        apellidos: data.apellidos,
        correo: data.correo,
        telefono: data.telefono
      })
      this.spinner.hide()
    },err=>{
      this.error = err;
      this.spinner.hide()
    })
  }

  formularioPersona = this.form.group({
    id:['',Validators.required],
    nombre: ['', Validators.required],
    apellidos: ['', [Validators.required, Validators.min(10), Validators.max(90)]],
    correo: ['', [Validators.required, Validators.email]],
    telefono: [null, [Validators.required, Validators.minLength(10)]]
  })

  //obtener los datos del formulario
  get nombre() { return this.formularioPersona.get('nombre'); }
  get apellidos() { return this.formularioPersona.get('apellidos'); }
  get correo() { return this.formularioPersona.get('correo'); }
  get telefono() { return this.formularioPersona.get('telefono'); }

  onUpdatePerson() {
    this.spinner.show()
    this.apiService.updatePerson(this.formularioPersona.value.id,this.formularioPersona.value).subscribe(data=>{      
      this.spinner.hide()
      this.formularioPersona.reset();
      this.successMessage = "Actualización correcta"
      //this.toastr.success("Actualización correcta","Exito");
    },err=>{
      this.spinner.hide()
      //this.
      console.log(err)
    })
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder,Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiServiceService } from 'src/app/services/api-service.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  mensajeExito:any;
  mensajeError:any;

  constructor(
    private form:FormBuilder,
    private apiService:ApiServiceService,
    private spinner:NgxSpinnerService
  ) { }

  ngOnInit(): void {
  }

  formularioPersona = this.form.group({
    Nombre: ['', Validators.required],
    Apellidos: ['', [Validators.required, Validators.min(10), Validators.max(90)]],
    Correo: ['',[Validators.required,Validators.email]],
    Telefono:['',[Validators.required, Validators.minLength(10), Validators.maxLength(10)]]
  })

  //obtener los datos del formulario
  get nombre() { return this.formularioPersona.get('Nombre'); }
  get apellidos() { return this.formularioPersona.get('Apellidos'); }
  get correo() { return this.formularioPersona.get('Correo'); }
  get telefono() { return this.formularioPersona.get('Telefono'); }

  onSubmitPerson(){    
    this.spinner.show();        
    this.apiService.createPerson(this.formularioPersona.value).subscribe(data=>{      
      this.mensajeExito = "Registro correcto"
      this.formularioPersona.reset();
      this.spinner.hide();
    },err=>{
      console.log(err)
      this.mensajeError = "Error. " + err.name;
      this.spinner.hide();
    })
  }

}

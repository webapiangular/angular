import { Component } from '@angular/core';

//Forms
import { FormBuilder, Validators } from '@angular/forms';

//Routes
import {Router} from '@angular/router';

//spinner
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'webapi';
  

  //variables
  error:any;
  identity = null;

  constructor(
    private form: FormBuilder, 
    private router: Router,
    private spinner: NgxSpinnerService
  ){
    this.spinner.show();
  }

  ngOnInit(): void {
    this.identity = localStorage.getItem('key')
    this.spinner.hide()
  }

  //Form
  formularioLogin = this.form.group({
    usuario:['',[Validators.required,Validators.email]],
    contrasenia:['',Validators.required]
  });

  //get data from form
  get usuario() { return this.formularioLogin.get('usuario'); }
  get contrasenia() { return this.formularioLogin.get('contrasenia'); } 
  

  onSubmitLogin(){    
    let {usuario,contrasenia} = this.formularioLogin.value;    
    if(usuario == "eduardo@coga.com" && contrasenia == "edu123"){
      localStorage.setItem('key','sesionExitosa')
      this.identity = localStorage.getItem('key')
    }else{
      this.error = "Datos de acceso incorrectos";
    }
  }

  onSubmitLogout(){
    this.identity = null;
    localStorage.removeItem('key')
    localStorage.clear()
    this.router.navigateByUrl('/read')
  }

}

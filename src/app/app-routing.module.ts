import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path:'',redirectTo:'read',pathMatch:'full'},
  { path: 'read', loadChildren: () => import('./components/crud/read/read.module').then(m => m.ReadModule) }, 
  { path: 'create', loadChildren: () => import('./components/crud/create/create.module').then(m => m.CreateModule) }, 
  { path: 'update/:id', loadChildren: () => import('./components/crud/update/update.module').then(m => m.UpdateModule) }, 
  { path: 'login', loadChildren: () => import('./components/login/login/login.module').then(m => m.LoginModule) },
  { path: '**', loadChildren: () => import('./components/notFound/not-found/not-found.module').then(m => m.NotFoundModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

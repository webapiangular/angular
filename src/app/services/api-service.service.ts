import { Injectable } from '@angular/core';
import { Persona } from '../models/persona';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { conf } from '../conf';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  url = conf.url;

  constructor(
    private http: HttpClient,
  ) { 

  }

  createPerson(persona:Persona):Observable<Persona>{    
    return this.http.post<Persona>(this.url + 'Persona', persona)
  }
  getPeople (): Observable<Persona[]> {  
    return this.http.get<Persona[]>(this.url + 'Persona');
  }

  getPerson (id:string): Observable<Persona> {  
    return this.http.get<Persona>(this.url + 'Persona/' + id);
  }

  deletePerson(id:string):Observable<Persona>{
    return this.http.delete<Persona>(this.url + 'Persona/' + id);
  }

  updatePerson(id:string,persona:Persona):Observable<Persona>{
    return this.http.put<Persona>(this.url + 'Persona/' + id, persona)
  }

}
